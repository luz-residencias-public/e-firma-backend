package com.intabi.efirmabackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.intabi.efirmabackend.dto.ResponseBody;
import com.intabi.efirmabackend.service.UsuariosService;

@RestController
@RequestMapping("/api/usuarios")
public class UsuariosRestController {
	
	@Autowired
	UsuariosService usuariosService;
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/", produces = "application/json")
	public ResponseBody getusuarios() {
	
		ResponseBody resp = new ResponseBody();
		
		return usuariosService.getUsuarios(resp);
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/", produces = "application/json", consumes = "application/json")
	public ResponseBody crearUsuario(@RequestBody PayloadUsuario payload) {
	
		ResponseBody resp = new ResponseBody();
		
		return usuariosService.createUsuario(payload, resp);
	}
	
	
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/{idUsuario}", produces = "application/json")
	public ResponseBody getusuario(@PathVariable Long idUsuario) {
	
		ResponseBody resp = new ResponseBody();
		
		return usuariosService.getUsuario(idUsuario, resp);
	}
	
	
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.PUT, path = "/{idUsuario}", produces = "application/json")
	public ResponseBody actualizarUsuario(@PathVariable Long idUsuario, @RequestBody PayloadUsuario payload){
	
		ResponseBody resp = new ResponseBody();
		
		return usuariosService.updateUsuario(idUsuario, payload, resp);
	}
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.DELETE, path = "/{idUsuario}", produces = "application/json")
	public ResponseBody borrarUsuarios(@PathVariable Long idUsuario) {
	
		ResponseBody resp = new ResponseBody();
		
		return usuariosService.deleteUsuario(idUsuario, resp);
	}
	

}

