package com.intabi.efirmabackend.controller;

public class PayloadDocumento {
	long id;
	String tipoDocumento;
	String tipoArchivo ;
	String nombreDocumento;
	String descripcion;
	String rutaDocumento;
	String contenido;
	String documentoBase64;
	String documentoFirmadoBase64;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getTipoArchivo() {
		return tipoArchivo;
	}
	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
	public String getNombreDocumento() {
		return nombreDocumento;
	}
	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getRutaDocumento() {
		return rutaDocumento;
	}
	public void setRutaDocumento(String rutaDocumento) {
		this.rutaDocumento = rutaDocumento;
	}
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public String getDocumentoBase64() {
		return documentoBase64;
	}
	public void setDocumentoBase64(String documentoBase64) {
		this.documentoBase64 = documentoBase64;
	}
	public String getDocumentoFirmadoBase64() {
		return documentoFirmadoBase64;
	}
	public void setDocumentoFirmadoBase64(String documentoFirmadoBase64) {
		this.documentoFirmadoBase64 = documentoFirmadoBase64;
	}
	
	
	
}