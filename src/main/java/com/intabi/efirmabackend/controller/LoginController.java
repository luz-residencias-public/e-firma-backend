package com.intabi.efirmabackend.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.intabi.efirmabackend.dto.LoginDTO;
import com.intabi.efirmabackend.dto.ResponseBody;
import com.intabi.efirmabackend.service.LoginService;

@RestController
@RequestMapping("/api/autenticacion")
public class LoginController {
	
	@Autowired
	LoginService loginService;
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, path = "/login", produces = "application/json")
	public ResponseBody login(@RequestBody LoginDTO payload) {
		ResponseBody responseBody = new ResponseBody();
		boolean response = loginService.login(payload);
		if(response == true) {
			responseBody.setData(payload);
			responseBody.setMessage("Autenticación exitosa");
			responseBody.setStatus("Success");
			
			return responseBody;
		}
		responseBody.setData(payload);
		responseBody.setMessage("Error al autenticar");
		responseBody.setStatus("Fail");
		
		return responseBody;
	}

	
}
