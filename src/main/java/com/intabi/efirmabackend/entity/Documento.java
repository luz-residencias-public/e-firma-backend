package com.intabi.efirmabackend.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "documento")
public class Documento {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
    
	@Column(name = "tipo_documento", nullable = false)
	String tipoDocumento;
	
	@Column(name = "tipo_archivo", nullable = false)
    String tipoArchivo;
	
	@Column(name = "nombre_documento", nullable = false)
    String nombreDocumento;
	
	@Column(name = "descripcion", nullable = false)
    String descripcion;
	
	@Column(name = "ruta_documento", nullable = false)
    String rutaDocumento;
	
	@Column(name = "contenido", nullable = false)
    String contenido;

	@Column(name = "documento_base64")
    String documentoBase64;
	
	@Column(name = "documento_firmado_base64", nullable = true)
    String documentoFirmadoBase64;
	
	@Column(name = "activo", nullable = false)
    boolean activo;
	
	@Column(name = "fh_creacion", nullable = false)
    Date fhCreacion;
	
	@Column(name = "fh_modificacion", nullable = false)
    Date fhModificacion;
	
	@Column(name = "usu_creacion", nullable = false)
    Long usuCreacion;
	
	@Column(name = "usu_modificacion", nullable = false)
    Long usuModificacion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public String getNombreDocumento() {
		return nombreDocumento;
	}

	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getRutaDocumento() {
		return rutaDocumento;
	}

	public void setRutaDocumento(String rutaDocumento) {
		this.rutaDocumento = rutaDocumento;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public Date getFhCreacion() {
		return fhCreacion;
	}

	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}

	public Date getFhModificacion() {
		return fhModificacion;
	}

	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}

	public Long getUsuCreacion() {
		return usuCreacion;
	}

	public void setUsuCreacion(Long usuCreacion) {
		this.usuCreacion = usuCreacion;
	}

	public Long getUsuModificacion() {
		return usuModificacion;
	}

	public void setUsuModificacion(Long usuModificacion) {
		this.usuModificacion = usuModificacion;
	}

	public String getDocumentoBase64() {
		return documentoBase64;
	}

	public void setDocumentoBase64(String documentoBase64) {
		this.documentoBase64 = documentoBase64;
	}

	public String getDocumentoFirmadoBase64() {
		return documentoFirmadoBase64;
	}

	public void setDocumentoFirmadoBase64(String documentoFirmadoBase64) {
		this.documentoFirmadoBase64 = documentoFirmadoBase64;
	}
	
	
}
