package com.intabi.efirmabackend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rol")
public class Rol {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name = "nombre_rol")
    private String nombreRol;
	
	@Column(name = "codigo_rol")
    private String codigoRol;
	
	@Column(name = "activo")
    private String activo;
    
    @Column(name = "fh_creacion")
    private String fhCreacion;
    
    @Column(name = "fh_modificacion")
    private String fhModificacion;
    
    @Column(name = "usu_creacion")
    private String usuCreacion;
    
    @Column(name = "usu_modificacion")
    private String usuModificacion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreRol() {
		return nombreRol;
	}

	public void setNombreRol(String nombreRol) {
		this.nombreRol = nombreRol;
	}

	public String getCodigoRol() {
		return codigoRol;
	}

	public void setCodigoRol(String codigoRol) {
		this.codigoRol = codigoRol;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getFhCreacion() {
		return fhCreacion;
	}

	public void setFhCreacion(String fhCreacion) {
		this.fhCreacion = fhCreacion;
	}

	public String getFhModificacion() {
		return fhModificacion;
	}

	public void setFhModificacion(String fhModificacion) {
		this.fhModificacion = fhModificacion;
	}

	public String getUsuCreacion() {
		return usuCreacion;
	}

	public void setUsuCreacion(String usuCreacion) {
		this.usuCreacion = usuCreacion;
	}

	public String getUsuModificacion() {
		return usuModificacion;
	}

	public void setUsuModificacion(String usuModificacion) {
		this.usuModificacion = usuModificacion;
	}
    
    
}
