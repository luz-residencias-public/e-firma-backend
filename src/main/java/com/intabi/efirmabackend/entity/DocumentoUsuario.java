package com.intabi.efirmabackend.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "documento_usuario")
public class DocumentoUsuario {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
	private Usuario idUsuario;
	
	@ManyToOne
    @JoinColumn(name = "id_documento", referencedColumnName = "id")
	private Documento idDocumento;
	
	@Column(name = "activo")
    private boolean activo;
    
    @Column(name = "fh_creacion")
    private Date fhCreacion;
    
    @Column(name = "fh_modificacion")
    private Date fhModificacion;
    
    @Column(name = "usu_creacion")
    private Long usuCreacion;
    
    @Column(name = "usu_modificacion")
    private Long usuModificacion;

}
