package com.intabi.efirmabackend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.intabi.efirmabackend.entity.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
    // Aquí puedes definir métodos adicionales de búsqueda, si es necesario
	Optional<Usuario> findByEmail(String email);
}