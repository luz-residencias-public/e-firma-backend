package com.intabi.efirmabackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.intabi.efirmabackend.entity.Documento;

@Repository
public interface DocumentoRepository extends JpaRepository<Documento, Long> {

}
