package com.intabi.efirmabackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.intabi.efirmabackend.entity.DocumentoUsuario;

@Repository
public interface DocumentoUsuarioRepository extends JpaRepository<DocumentoUsuario, Long> {

}