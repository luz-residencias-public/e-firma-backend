package com.intabi.efirmabackend.api.documento;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intabi.efirmabackend.controller.PayloadDocumento;
import com.intabi.efirmabackend.dto.ResponseBody;
import com.intabi.efirmabackend.entity.Documento;
import com.intabi.efirmabackend.repository.DocumentoRepository;


@Service
public class DocumentoService {

	@Autowired
	DocumentoRepository documentoRepository;
	
		
	public ResponseBody getDocumentos(ResponseBody resp) {
		List<Documento> documentosList = documentoRepository.findAll();
		
		
		if(documentosList.size()>0) {	
			
			PayloadDocumento payloadDocumento = null;
			List<PayloadDocumento> listDocumentosDto = new ArrayList<>();
			
			 for (Documento documento : documentosList) {
		            payloadDocumento = new PayloadDocumento();
		            payloadDocumento.setId(documento.getId());
		            payloadDocumento.setTipoDocumento(documento.getTipoDocumento());
		            payloadDocumento.setTipoArchivo(documento.getTipoArchivo());
		            payloadDocumento.setNombreDocumento(documento.getNombreDocumento());
		            payloadDocumento.setDescripcion(documento.getDescripcion());
		            payloadDocumento.setRutaDocumento(documento.getRutaDocumento());
		            payloadDocumento.setContenido(documento.getContenido());
					payloadDocumento.setDocumentoBase64(documento.getDocumentoBase64());
					payloadDocumento.setDocumentoFirmadoBase64(documento.getDocumentoFirmadoBase64());
		            listDocumentosDto.add(payloadDocumento);
			}
			
			resp.setMessage("Se han recuperado los datos de los documentos");
			resp.setStatus("OK");
			resp.setData(listDocumentosDto);
		}else {
			resp.setMessage("No existen datos en la tabla documentos");
			resp.setStatus("OK");
			resp.setData(null);
		}
		return resp;
	}
	
	
	

	public ResponseBody createDocumento(PayloadDocumento payload, ResponseBody resp) {
		Documento documento = new Documento();
		documento.setTipoDocumento(payload.getTipoDocumento());
		documento.setTipoArchivo(payload.getTipoArchivo());
		documento.setNombreDocumento(payload.getNombreDocumento());
		documento.setDescripcion(payload.getDescripcion());
		documento.setRutaDocumento(payload.getRutaDocumento());
		documento.setContenido(payload.getContenido());
		documento.setDocumentoBase64(payload.getDocumentoBase64());
		documento.setDocumentoFirmadoBase64(payload.getDocumentoBase64());
		documento.setActivo(true);
		documento.setFhCreacion(new Date());
		documento.setFhModificacion(new Date());
		documento.setUsuCreacion(1l);
		documento.setUsuModificacion(1L);

		try {
			documentoRepository.save(documento);
			resp.setMessage("El documento se ha creado satisfactoriamente");
			resp.setStatus("Succesful");
			resp.setData(documento);
			return resp;
		} catch (Exception e) {
			resp.setMessage("Error: " + e);
			resp.setStatus("Fail");
			resp.setData(null);
		}
		return resp;
	}
	
	public Documento getDocumento(Long idDocumento) {
		Optional<Documento> documento = documentoRepository.findById(idDocumento);
		if (documento.isPresent()) {
			return documento.get();
		}
		return null;
	}

	public ResponseBody getDocumento(Long idDocumento, ResponseBody resp) {
		Optional<Documento> documento = documentoRepository.findById(idDocumento);
		if (documento.isPresent()) {
			resp.setMessage("El documento se ha encontrado");
			resp.setStatus("Successful");
			resp.setData(documento.get());
		} else {
			resp.setMessage("El documento que buscas no existe");
			resp.setStatus("Fail");
		}
		return resp;
	}
	
	public boolean setDocumentoFirmado(Long idDocumento, String documentoFirmado, ResponseBody resp) {
		Optional<Documento> documentoOptional = documentoRepository.findById(idDocumento);
		if (documentoOptional.isPresent()) {
			Documento documentoExistente = documentoOptional.get();
			documentoExistente.setDocumentoFirmadoBase64(documentoFirmado);
			documentoExistente.setRutaDocumento("http://localhost:8083/api/documentos/download/" + documentoExistente.getId());
			documentoRepository.save(documentoExistente);
			return true;
		}
		resp.setMessage("El documento que buscas no existe");
		resp.setStatus("Fail");
		return false;
	}

	public ResponseBody updateDocumento(Long idDocumento, PayloadDocumento payload, ResponseBody resp) {
		Optional<Documento> documentoOptional = documentoRepository.findById(idDocumento);
		if (documentoOptional.isPresent()) {
			Documento documentoExistente = documentoOptional.get();			
			documentoExistente.setTipoDocumento(payload.getTipoDocumento());
			documentoExistente.setTipoArchivo(payload.getTipoArchivo());
			documentoExistente.setNombreDocumento(payload.getNombreDocumento());
			documentoExistente.setDescripcion(payload.getDescripcion());
			documentoExistente.setRutaDocumento(payload.getRutaDocumento());
			documentoExistente.setContenido(payload.getContenido());
			
			documentoRepository.save(documentoExistente);

			resp.setMessage("Documento actualizado correctamente");
			resp.setStatus("Successful");
			resp.setData(documentoExistente);			
		} else {
			resp.setMessage("El documento que buscas no existe");
			resp.setStatus("Fail");
		}
		return resp;
		
	}
	
	public ResponseBody deleteDocumento(Long idDocumento, ResponseBody resp) {
		Optional<Documento> documentoOptional = documentoRepository.findById(idDocumento);
		if(documentoOptional.isPresent()) {
			documentoRepository.delete(documentoOptional.get());
			resp.setMessage("Documento borrado");
			resp.setStatus("Successful");
			resp.setData(documentoOptional.get());							
		}else {
			resp.setMessage("El usuario que intentas borrar no existe");
			resp.setStatus("Fail");
		}
		return resp;
		
	}
	
	public File getDocFirmadoByDocumentoId(Long idDocumento) {
		Optional<Documento> documento = documentoRepository.findById(idDocumento);
		if (documento.isPresent()) {
			String docFirmadoBase64 = null;
			if(documento.get().getDocumentoFirmadoBase64()!=null) {
				docFirmadoBase64 = documento.get().getDocumentoFirmadoBase64();
			}else {
				docFirmadoBase64 = documento.get().getDocumentoBase64();
			}
			
			try {
				// Decodificar la cadena Base64 a bytes
				byte[] decodedBytes = java.util.Base64.getDecoder().decode(docFirmadoBase64);
				Random random = new Random();

				// Generar un número aleatorio de 5 dígitos
				int numeroAleatorio = random.nextInt(90000) + 10000;

				// Crear un archivo y escribir los bytes decodificados
				File outputFile = new File("docFirmado-" + numeroAleatorio + ".pdf");
				FileOutputStream outputStream = new FileOutputStream(outputFile);
				outputStream.write(decodedBytes);
				outputStream.close();

				return outputFile;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
		


