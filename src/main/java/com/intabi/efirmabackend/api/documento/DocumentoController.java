
package com.intabi.efirmabackend.api.documento;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.intabi.efirmabackend.controller.PayloadDocumento;
import com.intabi.efirmabackend.dto.ResponseBody;

@RestController
@RequestMapping("/api/documentos")
public class DocumentoController {
	
	@Autowired
	DocumentoService documentoService;	
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/", produces = "application/json")
	public ResponseBody getdocumentos() {
	
		ResponseBody resp = new ResponseBody();
		
		return documentoService.getDocumentos(resp);
	}
	
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/", produces = "application/json", consumes = "application/json")
	public ResponseBody crearDocumento(@RequestBody PayloadDocumento payload) {
	
		ResponseBody resp = new ResponseBody();
		
		return documentoService.createDocumento(payload, resp);
	}
	
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/{idDocumento}", produces = "application/json")
	public ResponseBody getdocumento(@PathVariable Long idDocumento) {
	
		ResponseBody resp = new ResponseBody();
		
		return documentoService.getDocumento(idDocumento, resp);
	}
	
		
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.PUT, path = "/{idDocumento}", produces = "application/json")
	public ResponseBody actualizarDocumento(@PathVariable Long idDocumento, @RequestBody PayloadDocumento payload){
	
		ResponseBody resp = new ResponseBody();
		
		return documentoService.updateDocumento(idDocumento, payload, resp);
	}
	
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.DELETE, path = "/{idDocumento}", produces = "application/json")
	public ResponseBody borrarDocumento(@PathVariable Long idDocumento) {
	
		ResponseBody resp = new ResponseBody();
		
		return documentoService.deleteDocumento(idDocumento, resp);
	}
	
	@GetMapping("/download/{documentId}")
	public ResponseEntity<InputStreamResource> downloadFile(@PathVariable Long documentId) throws IOException {

		File docFirmado = documentoService.getDocFirmadoByDocumentoId(documentId);

		if (docFirmado != null) {
			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + docFirmado.getName());

			InputStreamResource resource = new InputStreamResource(new FileInputStream(docFirmado));

			return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
					.contentLength(docFirmado.length()).body(resource);
		} else {
			// Handle case where documentId doesn't correspond to a file
			return ResponseEntity.notFound().build();
		}
	}
}
