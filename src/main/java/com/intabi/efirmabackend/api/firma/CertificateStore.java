/**
 * 
 */
package com.intabi.efirmabackend.api.firma;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;



public class CertificateStore {

	public static Certificado getCertificateFromFile(InputStream cert, String key) {

		Certificado certificado = new Certificado();
		try {

			KeyStore jks = KeyStore.getInstance("PKCS12");
			jks.load(cert, key.toCharArray());
			cert.close();

			String aliasJks = jks.aliases().nextElement();
			PrivateKey pk = (PrivateKey) jks.getKey(aliasJks, key.toCharArray());
			Certificate[] chain = jks.getCertificateChain(aliasJks);
			X509Certificate oPublicCertificate = (X509Certificate) chain[0];

			certificado.setAlias(oPublicCertificate.getSubjectDN().getName());
			certificado.setPublicCertificate(oPublicCertificate);
			certificado.setPrivateKey(pk);
			certificado.setCertificateChain(chain);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return certificado;
	}

	public static List<Certificado> listCertificateFromStore() {

		List<Certificado> listCertificado= new ArrayList<>();
		try {
			//KeyStore jks = KeyStore.getInstance("Windows-MY", "SunMSCAPI");//Para Windows
			KeyStore jks = KeyStore.getInstance("JKS");//Para Macbook
			jks.load(null, null);

			Enumeration<String> en = jks.aliases();
			while (en.hasMoreElements()) {
				Certificado certificado = new Certificado();
				String aliasKey = (String) en.nextElement();

				PrivateKey pk = (PrivateKey) jks.getKey(aliasKey, null);
				Certificate[] chain = jks.getCertificateChain(aliasKey);
				X509Certificate oPublicCertificate = (X509Certificate) chain[0];

				certificado.setAlias(oPublicCertificate.getSubjectDN().getName());
				certificado.setPublicCertificate(oPublicCertificate);
				certificado.setPrivateKey(pk);
				certificado.setCertificateChain(chain);

				listCertificado.add(certificado);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listCertificado;
	}

}
