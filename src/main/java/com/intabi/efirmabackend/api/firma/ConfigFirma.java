/**
 * 
 */
package com.intabi.efirmabackend.api.firma;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfDate;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPKCS7;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignature;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfString;

public class ConfigFirma {

	public static byte[] addSha256ToPdf(byte[] pdfContent, String sha256Hash) throws IOException, DocumentException {
		PdfReader reader = new PdfReader(pdfContent);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PdfStamper stamper = new PdfStamper(reader, baos);

		// Configurar fuente y color
		Font blueFont = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL, BaseColor.BLUE);

		// Recorrer todas las páginas y agregar el hash SHA256
		for (int i = 1; i <= reader.getNumberOfPages(); i++) {
			Rectangle pageSize = reader.getPageSize(i);
			float width = pageSize.getWidth();
			float height = pageSize.getHeight();

			PdfContentByte over = stamper.getOverContent(i);
			Phrase phrase = new Phrase(sha256Hash, blueFont);
			ColumnText.showTextAligned(over, Element.ALIGN_LEFT, phrase, width - 10, 200, 90);
		}

		stamper.close();
		reader.close();

		return baos.toByteArray();
	}

	public static byte[] addFirmaToPdf(byte[] pdfContent, String firmado, String noSerie, String fecha, int numFirma)
			throws IOException, DocumentException {
		PdfReader reader = new PdfReader(pdfContent);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PdfStamper stamper = new PdfStamper(reader, baos);

		// Configurar fuente y color
		Font blueFont = new Font(Font.FontFamily.HELVETICA, 5, Font.NORMAL, BaseColor.BLUE);

		// Recorrer todas las páginas y agregar el hash SHA256
		PdfContentByte over = stamper.getOverContent(reader.getNumberOfPages());
		Phrase phraseFirma = new Phrase(firmado, blueFont);
		Phrase phraseNoSerie = new Phrase(noSerie, blueFont);
		Phrase phraseFecha = new Phrase(fecha, blueFont);

		// A la izquierda el primer margen es de 9 y de ahí se incrementa en x
		// Abajo el primer margen es de 12 y de ahí se incrementa en y

		// Defino las constantes
		int level = 1;
		int nextLevelIncrease_Y = 28;
		int nextLevelIncrease_X = 170;

		int y1 = 22, y2 = 17, y3 = 12;
		int x = 9;

		if (numFirma <= 3) {
			level = 1;
		} else if (numFirma > 3 && numFirma <= 6) {
			level = 2;
			numFirma -= 3;
		} else if (numFirma > 6 && numFirma <= 9) {
			level = 3;
			numFirma -= 6;
		}

		nextLevelIncrease_Y *= (level - 1);
		x = x + (nextLevelIncrease_X * (numFirma - 1));
		y1 += nextLevelIncrease_Y;
		y2 += nextLevelIncrease_Y;
		y3 += nextLevelIncrease_Y;

		ColumnText.showTextAligned(over, Element.ALIGN_LEFT, phraseFirma, x, y1, 0);
		ColumnText.showTextAligned(over, Element.ALIGN_LEFT, phraseNoSerie, x, y2, 0);
		ColumnText.showTextAligned(over, Element.ALIGN_LEFT, phraseFecha, x, y3, 0);

		stamper.close();
		reader.close();

		return baos.toByteArray();
	}

	public static byte[] addFirmaRectanleToPdf(byte[] pdfContentFirmado, Certificado certificado, int numFirma)
			throws IOException, DocumentException, InvalidKeyException, NoSuchProviderException,
			NoSuchAlgorithmException, SignatureException {

		PdfReader reader = new PdfReader(pdfContentFirmado);
		ByteArrayOutputStream nuevoDocumento = new ByteArrayOutputStream();
		PdfStamper stamper = PdfStamper.createSignature(reader, nuevoDocumento, '\000', null, true);
		int lastPage = reader.getNumberOfPages();

		Date fechaFirma = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fechaFirma);

		PdfSignature signature = new PdfSignature(PdfName.ADOBE_PPKLITE, new PdfName("adbe.pkcs7.detached"));
		signature.setReason("Firma Digital");
		signature.setLocation("CIUDAD DE MEXICO");
		signature.setDate(new PdfDate(calendar));

		// stamper.close();
		PdfSignatureAppearance sap = stamper.getSignatureAppearance();
		sap.setSignDate(calendar);
		sap.setCryptoDictionary(signature);

		if (numFirma == 1) {
			float x = 9 * (72f / 96f);
			float y = 12 * (72f / 96f);
			float width = 215 * (72f / 96f);
			float height = 28 * (72f / 96f);

			sap.setVisibleSignature(new Rectangle(x, y, x + width, y + height), lastPage, null);
		} else if (numFirma == 2) {
			float x = 236 * (72f / 96f);
			float y = 12 * (72f / 96f);
			float width = 215 * (72f / 96f);
			float height = 28 * (72f / 96f);

			sap.setVisibleSignature(new Rectangle(x, y, x + width, y + height), lastPage, null);
		} else if (numFirma == 3) {
			float x = 462 * (72f / 96f);
			float y = 12 * (72f / 96f);
			float width = 215 * (72f / 96f);
			float height = 28 * (72f / 96f);

			sap.setVisibleSignature(new Rectangle(x, y, x + width, y + height), lastPage, null);
		} else if (numFirma == 4) {
			float x = 9 * (72f / 96f);
			float y = 50 * (72f / 96f);
			float width = 215 * (72f / 96f);
			float height = 28 * (72f / 96f);

			sap.setVisibleSignature(new Rectangle(x, y, x + width, y + height), lastPage, null);
		} else if (numFirma == 5) {
			float x = 236 * (72f / 96f);
			float y = 50 * (72f / 96f);
			float width = 215 * (72f / 96f);
			float height = 28 * (72f / 96f);

			sap.setVisibleSignature(new Rectangle(x, y, x + width, y + height), lastPage, null);
		} else if (numFirma == 6) {
			float x = 462 * (72f / 96f);
			float y = 50 * (72f / 96f);
			float width = 215 * (72f / 96f);
			float height = 28 * (72f / 96f);

			sap.setVisibleSignature(new Rectangle(x, y, x + width, y + height), lastPage, null);
		} else if (numFirma == 7) {
			float x = 9 * (72f / 96f);
			float y = 87 * (72f / 96f);
			float width = 215 * (72f / 96f);
			float height = 28 * (72f / 96f);

			sap.setVisibleSignature(new Rectangle(x, y, x + width, y + height), lastPage, null);
		} else if (numFirma == 8) {
			float x = 236 * (72f / 96f);
			float y = 87 * (72f / 96f);
			float width = 215 * (72f / 96f);
			float height = 28 * (72f / 96f);

			sap.setVisibleSignature(new Rectangle(x, y, x + width, y + height), lastPage, null);
		} else if (numFirma == 9) {
			float x = 462 * (72f / 96f);
			float y = 87 * (72f / 96f);
			float width = 215 * (72f / 96f);
			float height = 28 * (72f / 96f);

			sap.setVisibleSignature(new Rectangle(x, y, x + width, y + height), lastPage, null);
		}

		PdfContentByte layer2 = sap.getLayer(2);

		// Preparar para cerrar la firma
		int contentEstimated = 8192;
		HashMap<PdfName, Integer> exc = new HashMap<>();
		exc.put(PdfName.CONTENTS, contentEstimated * 2 + 2);
		sap.preClose(exc);

		// Sección de firma
		InputStream data = sap.getRangeStream();
		MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
		byte[] buf = new byte[contentEstimated];
		int n;
		while ((n = data.read(buf)) > 0) {
			messageDigest.update(buf, 0, n);
		}

		byte[] hash = messageDigest.digest();

		StringBuilder hexString = new StringBuilder();

		for (int i = 0; i < hash.length; i++) {
			String hex = Integer.toHexString(0xff & hash[i]);
			if (hex.length() == 1) {
				hexString.append('0');
			}
			hexString.append(hex);
		}

		String hashAsString = hexString.toString();

		System.out.println("hashGenerated: " + hashAsString);
		Calendar calendario = Calendar.getInstance();

		PdfPKCS7 sgn = new PdfPKCS7(certificado.getPrivateKey(), certificado.getCertificateChain(), null, "SHA-256",
				null, false);
		byte[] sh = sgn.getAuthenticatedAttributeBytes(hash, calendario, null);
		sgn.update(sh, 0, sh.length);

		byte[] encodedSig = sgn.getEncodedPKCS7(hash, calendario, null, null);// Este es el hash
		// END Sección de firma

		byte[] paddedSig = new byte[contentEstimated];
		System.arraycopy(encodedSig, 0, paddedSig, 0, encodedSig.length);

		PdfDictionary pdfDic = new PdfDictionary();
		pdfDic.put(PdfName.CONTENTS, new PdfString(paddedSig).setHexWriting(true));
		sap.close(pdfDic);

		reader.close();
		nuevoDocumento.flush();
		nuevoDocumento.close();

		System.out.println("encodedSig");
		String encodedString = Base64.getEncoder().encodeToString(encodedSig);
//        escribirArchivo(new String(encodedSig, "UTF-8"), encodedString); 
		System.out.println(new String(encodedSig, "UTF-8"));

		return nuevoDocumento.toByteArray();

	}

	public static byte[] firmaPdfBasico(String documento, Certificado certificado, int number) throws Exception {

		try {

			PdfReader reader = new PdfReader(Base64.getDecoder().decode(documento));
			ByteArrayOutputStream nuevoDocumento = new ByteArrayOutputStream();

			PdfStamper stp = PdfStamper.createSignature(reader, nuevoDocumento, '\000', null, true);
			PdfSignatureAppearance sap = stp.getSignatureAppearance();

			sap.setCrypto(certificado.getPrivateKey(), certificado.getCertificateChain(), null,
					PdfSignatureAppearance.WINCER_SIGNED);
			sap.setReason("Firma Digital");
			sap.setLocation("Tribunal Electoral de la CDMX");
			sap.setVisibleSignature(new Rectangle(100 * number, 100, 350, 200), 1, "sig" + number);
			stp.close();

			return nuevoDocumento.toByteArray();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] firmarPdfAvanzado(byte[] dataDoc, byte[] dataDocFirmado, Certificado certificado, int numero) throws Exception {
		try {

			PdfReader reader = new PdfReader(dataDocFirmado);
            AcroFields fields = reader.getAcroFields();
            List<String> signatureNames = fields.getSignatureNames();
            int signatureCount = signatureNames.size();
            if (signatureCount == 0) {
            	dataDocFirmado = addSha256ToPdf(dataDocFirmado, calcularSHA256(new String(dataDoc)));
            }
            
			Date fechaFirma = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(fechaFirma);

			String firmado = "Firmado por: "
					+ PdfPKCS7.getSubjectFields(certificado.getPublicCertificate()).getField("CN");
			String noSerie = "No. Serie: " + certificado.getPublicCertificate().getSerialNumber().toString();
			SimpleDateFormat dateformatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss Z");
			String fecha = "Fecha: " + dateformatter.format(fechaFirma);
			String firmaH = firmado + '\n' + noSerie + '\n' + fecha;

			dataDocFirmado = addFirmaToPdf(dataDocFirmado, firmado, noSerie, fecha, signatureCount + 1);

			dataDocFirmado = addFirmaRectanleToPdf(dataDocFirmado, certificado, signatureCount + 1);

			return dataDocFirmado;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String calcularSHA256(String texto) {
	    try {
	        MessageDigest md = MessageDigest.getInstance("SHA-256");
	        byte[] digest = md.digest(texto.getBytes(StandardCharsets.UTF_8));
	        return DatatypeConverter.printHexBinary(digest).toLowerCase();
	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
}
