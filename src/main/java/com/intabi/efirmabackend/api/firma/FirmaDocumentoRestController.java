package com.intabi.efirmabackend.api.firma;

import java.io.InputStream;
import java.util.Base64;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.intabi.efirmabackend.api.documento.DocumentoService;
import com.intabi.efirmabackend.dto.ResponseBody;
import com.intabi.efirmabackend.entity.Documento;

@Controller
@RequestMapping(path = "/api/firmas")
public class FirmaDocumentoRestController {
	
	@Autowired
	private FirmaDocumentoService firmaDocumentoSrvc;
	
	@Autowired
	DocumentoService documentoService;
	
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/firmar-documento", produces = "application/json")
	public ResponseEntity<ResponseBody> firmaDocumento(@RequestBody DocumentToFirmDTO dataToFirm) {
		ResponseBody resp = new ResponseBody();
		try {
			InputStream certificadoInpstrm = firmaDocumentoSrvc.base64ToInputStream(dataToFirm.getCertificadoBase64());
			Documento documento = documentoService.getDocumento(dataToFirm.getIdDocumento());
			
			byte[] docFirmadoBase64 = firmaDocumentoSrvc.firmarDoc(documento.getDocumentoBase64(), documento.getDocumentoFirmadoBase64(), certificadoInpstrm, dataToFirm.getContrasena(), 1);
			byte[] docFirmadoBase64Encode = Base64.getEncoder().encode(docFirmadoBase64);
			documentoService.setDocumentoFirmado(dataToFirm.getIdDocumento(), new String(docFirmadoBase64Encode), resp);
//			DocumentoDetailDTO docDetail = documentoService.getDocById(document.getId());
			DocumentoDetailDTO docDetail = new DocumentoDetailDTO();
			docDetail.setDocumentoFirmadoBase64(new String(docFirmadoBase64Encode));
			docDetail.setFhCreacion(new Date());
			docDetail.setIdDocumento(dataToFirm.getIdDocumento());
			docDetail.setUrlDescarga("http://localhost:8083/api/documentos/download/" + dataToFirm.getIdDocumento());
			
			resp.setData(docDetail);
			resp.setMessage("El documento se ha firmado satisfactoriamente");
			resp.setStatus("Ok");
			
			return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(resp);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
