package com.intabi.efirmabackend.api.firma;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DocumentoDetailDTO {
	Long idDocumento;
	String nombreDocumento;
	String llave1soloUso;
    Date fhCreacion;
    Date fhModificacion;
    int usuCreacion;
    int usuModificacion;
    String urlDescarga;
    List<Integer> firmantes;
    String documentoFirmadoBase64;
	public Long getIdDocumento() {
		return idDocumento;
	}
	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}
	public String getNombreDocumento() {
		return nombreDocumento;
	}
	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}
	public String getLlave1soloUso() {
		return llave1soloUso;
	}
	public void setLlave1soloUso(String llave1soloUso) {
		this.llave1soloUso = llave1soloUso;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public int getUsuCreacion() {
		return usuCreacion;
	}
	public void setUsuCreacion(int usuCreacion) {
		this.usuCreacion = usuCreacion;
	}
	public int getUsuModificacion() {
		return usuModificacion;
	}
	public void setUsuModificacion(int usuModificacion) {
		this.usuModificacion = usuModificacion;
	}
	public String getUrlDescarga() {
		return urlDescarga;
	}
	public void setUrlDescarga(String urlDescarga) {
		this.urlDescarga = urlDescarga;
	}
	public List<Integer> getFirmantes() {
		return firmantes;
	}
	public void setFirmantes(List<Integer> firmantes) {
		this.firmantes = firmantes;
	}
	public String getDocumentoFirmadoBase64() {
		return documentoFirmadoBase64;
	}
	public void setDocumentoFirmadoBase64(String documentoFirmadoBase64) {
		this.documentoFirmadoBase64 = documentoFirmadoBase64;
	}
    
    
	

}
