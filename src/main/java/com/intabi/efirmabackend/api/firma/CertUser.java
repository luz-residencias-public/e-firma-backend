package com.intabi.efirmabackend.api.firma;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

//import org.bouncycastle.asn1.x500.RDN;
//import org.bouncycastle.asn1.x500.X500Name;
//import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class CertUser {
//	InputStream inpStream;
//	X509Certificate certificate;
//	String serialnumber;
//	String derBase64;
//	String certSha256;
//	String emisorSerial;
//	String rfc;
//	String curp;
//	Date fechaRegistro;
//	Date fechaRevocacion;
//	String nombreComun;
//    String correoElectronico;
//    DTOIssuerSubjectData issuerData;
//    
//    
//	public CertUser(InputStream inpStream) {
//		super();
//		this.inpStream = inpStream;
//		setCertificateX509();
//		filllData();
//	}
//	
//	public CertUser(X509Certificate certificado) {
//		super();
//		this.certificate = certificado;
//		filllData();
//	}
//
//	public void setCertificateX509() {
//		X509Certificate certificate = null;
//		if (java.security.Security.getProvider("BC") == null) {
//            java.security.Security.addProvider(new BouncyCastleProvider());
//        }
//		
//		try {
//            CertificateFactory certFactory = CertificateFactory.getInstance("X.509", "BC");
//            certificate = (X509Certificate) certFactory.generateCertificate(this.inpStream);
//            this.inpStream.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//		this.certificate = certificate;
//	}
//	
//	public void filllData() {
//		X500Name subjectDN = new X500Name(this.certificate.getSubjectX500Principal().getName());
//		X500Name issuerDN = new X500Name(this.certificate.getIssuerX500Principal().getName());
//		
//
//        
//        RDN serialnumberIssuer = issuerDN.getRDNs()[1];
//        		
//        
//        this.issuerData = extractDataX500Name(issuerDN);
//		
//		//Se obtienen campos específicos del DN
//        RDN nombreComun_CN = subjectDN.getRDNs(BCStyle.CN)[0];
//        RDN serialnumber = subjectDN.getRDNs(BCStyle.SERIALNUMBER)[0];
//        RDN correoElectronico_E = subjectDN.getRDNs(BCStyle.E)[0];
//        RDN rfc = subjectDN.getRDNs()[1];
// 
//        this.nombreComun = nombreComun_CN.getFirst().getValue().toString();
//        this.curp = serialnumber.getFirst().getValue().toString();
//        this.nombreComun = nombreComun_CN.getFirst().getValue().toString();
//        this.correoElectronico = correoElectronico_E.getFirst().getValue().toString();
//        this.serialnumber = this.certificate.getSerialNumber().toString();
//        this.rfc = rfc.getFirst().getValue().toString();
//        
//        Date fhRegistroDate = this.certificate.getNotBefore();
//        Date fhRevocacionDate = this.certificate.getNotAfter();  
//        this.fechaRegistro = fhRegistroDate;
//        this.fechaRevocacion = fhRevocacionDate;
//        
//        String cerBase64 = convertCertToDer(this.certificate);
//        this.derBase64 = cerBase64;
//        this.certSha256 = calcularSHA256(cerBase64);
//        this.emisorSerial = serialnumberIssuer.getFirst().getValue().toString();
//	}
//	
//	public String calcularSHA256(String texto) {
//	    try {
//	        MessageDigest md = MessageDigest.getInstance("SHA-256");
//	        byte[] digest = md.digest(texto.getBytes(StandardCharsets.UTF_8));
//	        return DatatypeConverter.printHexBinary(digest).toLowerCase();
//	    } catch (NoSuchAlgorithmException e) {
//	        e.printStackTrace();
//	        return null;
//	    }
//	}
//	
//	public String convertCertToDer(X509Certificate certificado) {
//		byte[] derEncoded;
//		try {
//			derEncoded = certificado.getEncoded();
//			String base64String = Base64.getEncoder().encodeToString(derEncoded);
//			return base64String;
//		} catch (CertificateEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
//	}
//	
//	public DTOIssuerSubjectData extractDataX500Name(X500Name xname) {
//		DTOIssuerSubjectData subjectDataDTO = new DTOIssuerSubjectData();
//		
//        if(xname.getRDNs(BCStyle.UnstructuredName).length>=1) {
//        	subjectDataDTO.setResponsable((xname.getRDNs(BCStyle.UnstructuredName)[0].getFirst().getValue().toString()).replace("responsable:", "").trim());
//        }
//        if(xname.getRDNs(BCStyle.UNIQUE_IDENTIFIER).length>=1) {
//        	subjectDataDTO.setSerialnumberIssuer(xname.getRDNs(BCStyle.UNIQUE_IDENTIFIER)[0].getFirst().getValue().toString());
//        }
//        if(xname.getRDNs(BCStyle.L).length>=1) {
//        	subjectDataDTO.setIssuer_l(xname.getRDNs(BCStyle.L)[0].getFirst().getValue().toString());
//        }
//        if(xname.getRDNs(BCStyle.ST).length>=1) {
//        	subjectDataDTO.setIssuer_s(xname.getRDNs(BCStyle.ST)[0].getFirst().getValue().toString());
//        }
//        if(xname.getRDNs(BCStyle.C).length>=1) {
//        	subjectDataDTO.setIssuer_c(xname.getRDNs(BCStyle.C)[0].getFirst().getValue().toString());
//        }
//        if(xname.getRDNs(BCStyle.POSTAL_CODE).length>=1) {
//        	subjectDataDTO.setIssuer_postalcode(xname.getRDNs(BCStyle.POSTAL_CODE)[0].getFirst().getValue().toString());
//        }
//        if(xname.getRDNs(BCStyle.STREET).length>=1) {
//        	subjectDataDTO.setIssuer_street(xname.getRDNs(BCStyle.STREET)[0].getFirst().getValue().toString());
//        }
//        if(xname.getRDNs(BCStyle.EmailAddress).length>=1) {
//        	subjectDataDTO.setIssuer_e(xname.getRDNs(BCStyle.EmailAddress)[0].getFirst().getValue().toString());
//        }
//        if(xname.getRDNs(BCStyle.OU).length>=1) {
//        	subjectDataDTO.setIssuer_ou(xname.getRDNs(BCStyle.OU)[0].getFirst().getValue().toString());
//        }
//        if(xname.getRDNs(BCStyle.O).length>=1) {
//        	subjectDataDTO.setIssuer_o(xname.getRDNs(BCStyle.O)[0].getFirst().getValue().toString());
//        }
//        if(xname.getRDNs(BCStyle.CN).length>=1) {
//        	subjectDataDTO.setIssuer_cn(xname.getRDNs(BCStyle.CN)[0].getFirst().getValue().toString());
//        }
//        
//        return subjectDataDTO;
//	}
//	
//	
//	
//	public InputStream getInpStream() {
//		return inpStream;
//	}
//
//	public void setInpStream(InputStream inpStream) {
//		this.inpStream = inpStream;
//	}
//
//	public X509Certificate getCertificate() {
//		return certificate;
//	}
//
//	public void setCertificate(X509Certificate certificate) {
//		this.certificate = certificate;
//	}
//
//	public String getSerialnumber() {
//		return serialnumber;
//	}
//
//	public void setSerialnumber(String serialnumber) {
//		this.serialnumber = serialnumber;
//	}
//
//	public String getDerBase64() {
//		return derBase64;
//	}
//
//	public void setDerBase64(String derBase64) {
//		this.derBase64 = derBase64;
//	}
//
//	public String getCertSha256() {
//		return certSha256;
//	}
//
//	public void setCertSha256(String certSha256) {
//		this.certSha256 = certSha256;
//	}
//
//	public String getEmisorSerial() {
//		return emisorSerial;
//	}
//
//	public void setEmisorSerial(String emisorSerial) {
//		this.emisorSerial = emisorSerial;
//	}
//
//	public String getRfc() {
//		return rfc;
//	}
//
//	public void setRfc(String rfc) {
//		this.rfc = rfc;
//	}
//
//	public String getCurp() {
//		return curp;
//	}
//
//	public void setCurp(String curp) {
//		this.curp = curp;
//	}
//
//	public Date getFechaRegistro() {
//		return fechaRegistro;
//	}
//
//	public void setFechaRegistro(Date fechaRegistro) {
//		this.fechaRegistro = fechaRegistro;
//	}
//
//	public Date getFechaRevocacion() {
//		return fechaRevocacion;
//	}
//
//	public void setFechaRevocacion(Date fechaRevocacion) {
//		this.fechaRevocacion = fechaRevocacion;
//	}
//
//	public String getNombreComun() {
//		return nombreComun;
//	}
//
//	public void setNombreComun(String nombreComun) {
//		this.nombreComun = nombreComun;
//	}
//
//	public String getCorreoElectronico() {
//		return correoElectronico;
//	}
//
//	public void setCorreoElectronico(String correoElectronico) {
//		this.correoElectronico = correoElectronico;
//	}
//
//	public DTOIssuerSubjectData getIssuerData() {
//		return issuerData;
//	}
//
//	public void setIssuerData(DTOIssuerSubjectData issuerData) {
//		this.issuerData = issuerData;
//	}
	
}
