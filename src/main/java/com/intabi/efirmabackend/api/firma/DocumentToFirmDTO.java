package com.intabi.efirmabackend.api.firma;

public class DocumentToFirmDTO {
	String documentoBase64;
	String nombre;
	String certificadoBase64;
	String llaveBase64;
	String contrasena;
	Long idDocumento;
	public String getDocumentoBase64() {
		return documentoBase64;
	}
	public void setDocumentoBase64(String documentoBase64) {
		this.documentoBase64 = documentoBase64;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCertificadoBase64() {
		return certificadoBase64;
	}
	public void setCertificadoBase64(String certificadoBase64) {
		this.certificadoBase64 = certificadoBase64;
	}
	public String getLlaveBase64() {
		return llaveBase64;
	}
	public void setLlaveBase64(String llaveBase64) {
		this.llaveBase64 = llaveBase64;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public Long getIdDocumento() {
		return idDocumento;
	}
	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}
	
	
		
}
