package com.intabi.efirmabackend.api.firma;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfDate;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPKCS7;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignature;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfString;

@Service
public class FirmaDocumentoService {
	

	
	public InputStream base64ToInputStream(String certificadoBase64) {
		// Decodificar la cadena Base64
        byte[] certificadoBynary = Base64.getDecoder().decode(certificadoBase64);
        
        // Crear un InputStream a partir del array de bytes
        InputStream cert = new ByteArrayInputStream(certificadoBynary);
        return cert;
	}

	public static String convertToBase64(MultipartFile multipartFile) throws Exception {
        byte[] fileBytes = multipartFile.getBytes();
        byte[] base64Bytes = Base64.getEncoder().encode(fileBytes);
        return new String(base64Bytes);
    }
	
	public byte[] firmarDoc(String documentoBase64, String documentoFirmadoBase64, InputStream cert, String password, int number) throws Exception {
		Certificado certificado= CertificateStore.getCertificateFromFile(cert, password);
		byte[] data = ConfigFirma.firmarPdfAvanzado(Base64.getDecoder().decode(documentoBase64), 
				Base64.getDecoder().decode(documentoFirmadoBase64), certificado, number);
		return data;
	}
	
	

}
