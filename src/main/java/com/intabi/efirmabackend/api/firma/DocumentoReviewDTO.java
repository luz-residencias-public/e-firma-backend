package com.intabi.efirmabackend.api.firma;

import java.util.Date;
import java.util.List;

public class DocumentoReviewDTO {
	int idDocumento;
	String nombreDocumento;
    Date fhCreacion;
    Date fhModificacion;
    int usuCreacion;
    int usuModificacion;
    List<Integer> firmantes;
    //String documentoFirmadoBase64;
	public int getIdDocumento() {
		return idDocumento;
	}
	public void setIdDocumento(int idDocumento) {
		this.idDocumento = idDocumento;
	}
	public String getNombreDocumento() {
		return nombreDocumento;
	}
	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public int getUsuCreacion() {
		return usuCreacion;
	}
	public void setUsuCreacion(int usuCreacion) {
		this.usuCreacion = usuCreacion;
	}
	public int getUsuModificacion() {
		return usuModificacion;
	}
	public void setUsuModificacion(int usuModificacion) {
		this.usuModificacion = usuModificacion;
	}
	public List<Integer> getFirmantes() {
		return firmantes;
	}
	public void setFirmantes(List<Integer> firmantes) {
		this.firmantes = firmantes;
	}
    
    
    
    

}
