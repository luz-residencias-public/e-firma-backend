package com.intabi.efirmabackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EfirmaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(EfirmaBackendApplication.class, args);
	}

}
