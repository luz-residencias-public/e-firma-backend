package com.intabi.efirmabackend.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intabi.efirmabackend.dto.LoginDTO;
import com.intabi.efirmabackend.entity.Usuario;
import com.intabi.efirmabackend.repository.UsuarioRepository;

@Service
public class LoginService {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	public boolean login(LoginDTO payload) {
		Optional<Usuario> usuario = usuarioRepository.findByEmail(payload.getEmail());
		if(usuario.isPresent()) {
			if(payload.getPassword().equals(usuario.get().getPassword())) {
				return true;
			}
		}
		return false;
	}

}
