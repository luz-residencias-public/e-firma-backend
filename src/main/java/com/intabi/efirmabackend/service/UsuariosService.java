package com.intabi.efirmabackend.service;

import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intabi.efirmabackend.controller.PayloadUsuario;
import com.intabi.efirmabackend.dto.ResponseBody;
import com.intabi.efirmabackend.dto.UsuarioDTO;
import com.intabi.efirmabackend.entity.Rol;
import com.intabi.efirmabackend.entity.Usuario;
import com.intabi.efirmabackend.repository.RolRepository;
import com.intabi.efirmabackend.repository.UsuarioRepository;

@Service
public class UsuariosService {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	RolRepository rolRepository;
	
	public ResponseBody deleteUsuario(Long idUsuario, ResponseBody resp) {
		Optional<Usuario> usuarioOptional = usuarioRepository.findById(idUsuario);
		if(usuarioOptional.isPresent()) {
			usuarioRepository.delete(usuarioOptional.get());
			resp.setMessage("Usuario borrado");
			resp.setStatus("Successful");
			resp.setData(usuarioOptional.get());							
		}else {
			resp.setMessage("El usuario que intentas borrar no existe");
			resp.setStatus("Fail");
		}
		return resp;
		
	}
	
	public ResponseBody updateUsuario(Long idUsuario, PayloadUsuario payload, ResponseBody resp) {
		Optional<Usuario> usuarioOptional = usuarioRepository.findById(idUsuario);
		if(usuarioOptional.isPresent()) {
			Usuario usuarioExistente = usuarioOptional.get();
			usuarioExistente.setNombre(payload.getNombre());
			usuarioExistente.setApellidop(payload.getApellidop());
			usuarioExistente.setApellidom(payload.getApellidom());
	        usuarioExistente.setEmail(payload.getEmail());
	        usuarioExistente.setPassword(payload.getPassword());
	        usuarioExistente.setApellidom(payload.getApellidom());
	        usuarioExistente.setNumExpediente(payload.getNumExpediente());
	        
	        usuarioRepository.save(usuarioExistente);
	        
			resp.setMessage("Usuario actualizado correctamente");
			resp.setStatus("Successful");
			resp.setData(usuarioExistente);
		}else {
			resp.setMessage("El usuario que buscas no existe");
			resp.setStatus("Fail");
		}
		return resp;
	}
	
	public ResponseBody getUsuario(Long idUsuario, ResponseBody resp) {
		Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
		if(usuario.isPresent()) {
			resp.setMessage("El usuario se ha encontrado");
			resp.setStatus("Successful");
			resp.setData(usuario.get());
		}else {
			resp.setMessage("El usuario que buscas no existe");
			resp.setStatus("Fail");
		}
		return resp;
	}

	public ResponseBody getUsuarios(ResponseBody resp) {
		List<Usuario> usuariosList = usuarioRepository.findAll();
		
		
		if(usuariosList.size()>0) {

			UsuarioDTO usuarioDto = null;
			List<UsuarioDTO> listUsuariosDto = new ArrayList<UsuarioDTO>();
			
			for(Usuario usuario: usuariosList) {
				usuarioDto = new UsuarioDTO();
				usuarioDto.setId(usuario.getId());
				usuarioDto.setEmail(usuario.getEmail());
				usuarioDto.setExpediente(usuario.getNumExpediente());
				usuarioDto.setNombre(usuario.getNombre()+" "+usuario.getApellidop()+" "+usuario.getApellidom());
				usuarioDto.setRol(usuario.getRol().getCodigoRol());
				
				listUsuariosDto.add(usuarioDto);
			}
			
			resp.setMessage("Se han recuperado los datos de los usuarios");
			resp.setStatus("OK");
			resp.setData(listUsuariosDto);
		}else {
			resp.setMessage("No existen datos en la tabla usuarios");
			resp.setStatus("OK");
			resp.setData(null);
		}
		return resp;
	}
	
	public ResponseBody createUsuario(PayloadUsuario payload, ResponseBody resp) {
		Usuario usuario = new Usuario();
		usuario.setNombre(payload.getNombre());
		usuario.setApellidop(payload.getApellidop());
		usuario.setApellidom(payload.getApellidom());
		usuario.setEmail(payload.getEmail());
		usuario.setPassword(payload.getPassword());
		usuario.setNumExpediente(payload.getNumExpediente());
		
		Optional<Rol> rol = rolRepository.findByCodigoRol(payload.getCodigoRol());
		if(rol.isPresent()) {
			usuario.setRol(rol.get());
			
			usuario.setActivo(true);
			usuario.setFhCreacion(new Date());
			usuario.setFhModificacion(new Date());
			usuario.setUsuCreacion(1L);
			usuario.setUsuModificacion(1L);
			
			try {
				usuarioRepository.save(usuario);
				resp.setMessage("El usuario se ha creado satisfactoriamente");
				resp.setStatus("Succesful");
				resp.setData(usuario);
				return resp;
			}catch(Exception e) {
				resp.setMessage("Error: "+e);
				resp.setStatus("Fail");
				resp.setData(null);
				return resp;
			}
		}else {
			resp.setMessage("No se encontró el código de rol específicado");
			resp.setStatus("Fail");
			return resp;
		}
		
	}
}
